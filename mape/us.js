function openPage (pageName, elmnt, color) {
    var i, pagecontent, pagelinks;

    pagecontent = document.getElementsByClassName("pagecontent");
    for (i = 0; i < pagecontent.length; i++) {
        pagecontent[i].style.display = "none";
    }

    pagelinks = document.getElementsByClassName("pagelink");
        for (i = 0; i < pagelinks.length; i++) {
        pagelinks[i].style.backgroundColor = "";
    }

    document.getElementById(pageName).style.display = "block";
    elmnt.style.backgroundColor = color;
}

document.getElementById("defaultOpen").click();