CREATE DATABASE IF NOT EXISTS merch ;
USE merch ;

  `id_user` int NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `surname` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_user`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `merch`.`product` (
  `id_prdct` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(150) NOT NULL,
  `dscrpt` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id_prdct`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `merch`.`Choice` (
  `idChoice` INT NOT NULL AUTO_INCREMENT,
  `idusr` INT NOT NULL,
  `idpr` INT NOT NULL,
  `size` VARCHAR(45) NOT NULL,
  `price` DECIMAL(10,2) NOT NULL,
  PRIMARY KEY (`idChoice`),
  INDEX `User_idx` (`idusr` ASC) VISIBLE,
  INDEX `product_idx` (`idpr` ASC) VISIBLE,
  CONSTRAINT `User`
    FOREIGN KEY (`idusr`)
    REFERENCES `merch`.`User` (`id_user`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `product`
    FOREIGN KEY (`idpr`)
    REFERENCES `merch`.`product` (`id_prdct`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
