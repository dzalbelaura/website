# About the Website

This is a website for Students council of RTU Faculty of Electronics and Telecommunications (shorter - FET SP). In the website you can read all the basic information about FET SP, about news, actualities and projects their are making, also you can see a lot of pictures from lots of events. FET SP has their own merch, so you can get some wearing stuff with that.

# How to get started
1) Download and install following:
*  Git - https://git-scm.com/downloads
*  Node.js (version 13) - https://nodejs.org/en/download/current/

2) To run the project in opened terminal write the following:
*  npm install
*  npm start

3) Open the website in a browser:
*  http://localhost:3000/firstpage.html